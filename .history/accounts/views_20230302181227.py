from django.shortcuts import render
from django.contrib.auth import login, authenticate
from accounts.forms import LoginForm


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
    if form.is_valid():
        username = form.cleaned_data[u]
