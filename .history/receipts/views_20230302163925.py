from django.shortcuts import render
from receipts.models import ExpenseCategory, Account, Receipt


def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {
        "receipt": receipt,
        "category"
    }
    return render(request, "receipts/list.html", context)
