from django.shortcuts import render
from receipts.models import ExpenseCategory, Account, Receipt
from .models import Receipt
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipt,
    }
    return render(request, "receipts/list.html", context)



