from django.db import models


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        "User",
        related_name= "categories"
        on_del
    )
