from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ExpenseCategoryAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.AccountAdmin):
    pass


@admin.register
