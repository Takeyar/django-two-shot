from django.db import models


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        "User",
        related_name="categories",
        on_delete=models.CASCADE
    )


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        "User",
        related_name="accounts",
        on_delete=models.CASCADE
    )


class Receipt(models.Model)
