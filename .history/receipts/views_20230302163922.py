from django.shortcuts import render
from receipts.models import ExpenseCategory, Account, Receipt


def receipt_list(request):
    receipt = Receipt.objects.all()
    context = {
        "receipt": receipt,
        "categor"
    }
    return render(request, "receipts/list.html", context)
