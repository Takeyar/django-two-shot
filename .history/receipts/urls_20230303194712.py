from django.contrib import admin
from django.urls import path
from receipts.views import receipt_list, create_receipt, category_list, account_list


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt")
    path("")
]
