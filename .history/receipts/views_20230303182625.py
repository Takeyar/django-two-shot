from django.shortcuts import render
from receipts.models import ExpenseCategory, Account, Receipt
from .models import Receipt
from django.contrib.auth.decorators import login_required
from rece


@login_required
def receipt_list(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm
