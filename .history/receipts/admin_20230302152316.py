from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    pass
